import React from 'react';
import './AutoComplete.css'

class AutoComplete extends React.Component {
    state = {
        // The active selection's index
        activeSuggestion: 0,
        // The suggestions that match the user's input
        filteredSuggestions: [],
        // Whether or not the suggestion list is shown
        showSuggestions: false,
        // What the user has entered
        userInput: ''
    }

    handleInputChange = e => {
        const {suggestions} = this.props;
        const userInput = e.currentTarget.value;
        // Filters out suggestions that DON'T contain the user's input
        const filteredSuggestions = suggestions.filter(suggestion => {
            return suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1;
        })


        this.setState({
            activeSuggestion: 0,
            filteredSuggestions,
            showSuggestions: true,
            userInput: e.currentTarget.value
        });
    }

    handleClick = e => {
        // Update the user input and reset the rest of the state
        this.setState({
            activeSuggestion: 0,
            filteredSuggestions: [],
            showSuggestions: false,
            userInput: e.currentTarget.innerText
        });
    }

    onKeyDown = e => {
        const {activeSuggestion, filteredSuggestions} = this.state;
        // User pressed the enter key, update the input and close the suggestions window
        if (e.keyCode === 13) {
            this.setState({
                activeSuggestion: 0,
                showSuggestions: false,
                userInput: filteredSuggestions[activeSuggestion]
            });
        }
        // User pressed the up arrow, decrement the index
        else if (e.keyCode === 38) {
            if (activeSuggestion === 0) return;
            this.setState({activeSuggestion: activeSuggestion - 1});
        }
        // User pressed the down arrow, increment the index
        else if (e.keyCode === 40) {
            if (activeSuggestion - 1 === filteredSuggestions.length) return;
            this.setState({activeSuggestion: activeSuggestion + 1});
        }

        // User pressed the esc key, close suggestions window
        else if (e.keyCode === 27) {
            this.setState({
                activeSuggestion: 0,
                showSuggestions: false,
            });
        }
    };

    highlightMatchedText(suggestionItem, toHighlight) {
        // Split on highlight term and include term into parts, highlight matched part
        const parts = suggestionItem.split(new RegExp(`(${toHighlight})`, 'gi'));
        return (
            <span> {parts.map((part, i) =>
                <span key={i} style={part.toLowerCase() === toHighlight.toLowerCase() ? {fontWeight: 'bold'} : {}}>
            {part}
        </span>)
            } </span>
        );
    }

    closeSuggestions = (e) => {
        this.setState({
            activeSuggestion: 0,
            showSuggestions: false,
        })
    }

    render() {
        const {
            handleInputChange,
            handleClick,
            onKeyDown,
            closeSuggestions,
            highlightMatchedText,
            state: {
                activeSuggestion,
                filteredSuggestions,
                showSuggestions,
                userInput
            }
        } = this;

        let suggestionList;
        if (userInput && showSuggestions) { // If user is currently typing
            if (filteredSuggestions.length) {
                suggestionList = (
                    <ul className="suggestions">
                        {filteredSuggestions.map((suggestion, index) => {
                            let className = index === activeSuggestion ? 'suggestion-active' : '';
                            return (
                                <li
                                    key={suggestion + Math.random()}
                                    className={className}
                                    onClick={handleClick}
                                >
                                    {highlightMatchedText(suggestion, userInput)}
                                </li>
                            )
                        })}
                    </ul>
                )
            } else {
                suggestionList = (
                    <div className="no-suggestions">
                        <em>City wasn't found</em>
                    </div>
                )
            }
        }

        return (
            <React.Fragment>
                <input
                    type="text"
                    onChange={handleInputChange}
                    onKeyDown={onKeyDown}
                    onBlur={() => closeSuggestions}
                    value={userInput}
                />
                {suggestionList}
            </React.Fragment>
        )
    }
}

export default AutoComplete;