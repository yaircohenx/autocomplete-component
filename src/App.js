import React from 'react';
import './App.css';
import AutoComplete from './AutoComplete';

class App extends React.Component {
    state = {
        cities: null,
        hasData: false
    }
    // Get data of all Israeli cities
    componentDidMount() {
        fetch('https://raw.githubusercontent.com/royts/israel-cities/master/israel-cities.json')
            .then(resp => resp.json())
            .then((data) => {
                const cities = [];
                for (let i = 1; i < data.length; i++) {
                    const cityName = this.formatCityName(data[i].english_name);
                    cities.push(cityName)
                }
                this.setState({
                    cities,
                    hasData: true
                });
            })
    }
    // Turns string from all uppercase to uppercase in first letter only
    formatCityName = cityName => {
        return cityName.toLowerCase().replace(/^\w|\s\w/g, function (letter) {
            return letter.toUpperCase();
        })
    }

    render() {
        const {cities, hasData} = this.state;
        if (!hasData) {
            return <div className="loading">Loading...</div>
        } else {
            return (
                <div className="App">
                    <div className="container">
                        <h1 className="welcome-title">Welcome, Gilad</h1>
                        <div className="form-container">
                            <h2 className="steps-title">Step One</h2> <br/>
                            <span>Where are you from?</span> <br/>
                            <AutoComplete
                                suggestions={cities}/>
                            <button>Submit</button>
                            <br/>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default App;
